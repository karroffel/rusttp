fn main() {
    let server = tiny_http::Server::http("0.0.0.0:1337").unwrap();

    loop {
        let request = server.recv().unwrap();

        // The URL given by tiny_http is giving only the path part.
        // This is not very nice code but it works!
        let url_str = request
            .url()
            .trim_start_matches(|c| c != '?')
            .trim_start_matches('?');

        let query_pairs = url::form_urlencoded::parse(url_str.as_bytes());

        let list = query_pairs
            .map(|(key, value)| {
                if !value.is_empty() {
                    format!(
                        "<li>{} = {}</li>\n",
                        ammonia::clean_text(&key),
                        ammonia::clean_text(&value)
                    )
                } else {
                    format!("<li>{}</li>\n", ammonia::clean_text(&key))
                }
            })
            .fold(String::new(), |acc, val| acc + &val);

        let body = format!("<ul>\n{}</ul>", list);

        let response = tiny_http::Response::from_string(body).with_header(
            tiny_http::Header::from_bytes(&b"Content-Type"[..], &b"text/html"[..]).unwrap(),
        );

        request.respond(response).unwrap();
    }
}
